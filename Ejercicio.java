package cl.riffo.fernando;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Ejercicio {

	public static void main(String[] args) {
		searchNumber();
	}

	public static void searchNumber() {

		ejercicio1(51);

		ejercicio2();
		
		/**
		El numero 51 existe en la posicion: 11 del Array
		Lista desordenada
		[54, 53, 2, 1, 5, 98, 73, 86, 98, 94, 1, 2, 3, 2]
		Lista orden descendente
		[98, 98, 94, 86, 73, 54, 53, 5, 3, 2, 2, 2, 1, 1]
		Lista orden ascendente
		[1, 1, 2, 2, 2, 3, 5, 53, 54, 73, 86, 94, 98, 98]
		*/

	}

	private static void ejercicio1(Number num) {
		/** Ejercicio 1 Busqueda Numero 51 */
		Integer array[] = new Integer[] { 1, 3, 13, 15, 17, 19, 21, 23, 31, 34, 40, 51, 54, 68 };

		boolean exist = Arrays.stream(array).anyMatch(x -> x == num);
		if (exist) {
			Integer posicion = Arrays.asList(array).indexOf(num);
			System.out.println("El numero " + num + " existe en la posicion: " + posicion + " del Array");
		} else {
			System.out.println("El numero no existe en el Array");
		}

	}

	private static void ejercicio2() {
		/** Ejercicio 2 Ordenamiento */
		Integer array[] = new Integer[] { 54, 53, 2, 1, 5, 98, 73, 86, 98, 94, 1, 2, 3, 2 };
		List<Integer> listaDesordenada = Arrays.asList(array);

		/** Lista desordenada*/
		System.out.println("Lista desordenada");
		System.out.println(listaDesordenada);
		
		/** Lista orden descendente*/
		Collections.sort(listaDesordenada, Collections.reverseOrder());
		System.out.println("Lista orden descendente");
		System.out.println(listaDesordenada);
		/** Lista orden ascendente*/
		Collections.sort(listaDesordenada);
		System.out.println("Lista orden ascendente");
		System.out.println(listaDesordenada);
	}

}
