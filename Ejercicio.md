#Proyecto Postulacion

pasos a seguir para compilar el proyecto


## Angular

primero instalar las fuentes de la carpeta frontendpostulacionFR:

npm install

reemplazar la url de host en el archivo 'environment.prod.ts'

API: 'http://localhost:8080',

luego compilar el proyecto angular

ng build --prod esto copia automaticamente las fuentes generadas a la ruta "../backendpostulacionFR/src/main/resources/static/"

## Spring boot

Compilar con maven la carpeta 'backendpostulacionFR'

mvn clean compile package

este generara el archivo backendpostulacionFR-0.0.1.jar

## Docker

creacion de imagen de mysql

docker pull mysql:5.7

luego iniciar la instancia de mysql

docker run --name mysql-standalone -e MYSQL_ROOT_PASSWORD=sa -e MYSQL_DATABASE=friffo -e MYSQL_USER=admin -e MYSQL_PASSWORD=12345 -d mysql:5.7

creacion de imagen docker a partir del proyecto, ejecutar comando en path 'backendpostulacionFR' donde se encuentra el archivo 'dockerfile'

docker build -t postulacionfr .       

luego iniciar el contenedor docker con la app y enlazarlo al contenedor de mysql

docker run -d -p 8080:8080 --name apppostulacionfr --link mysql-standalone postulacionfr

ahora es posible revisar la aplicacion en 

##http://localhost:8080
