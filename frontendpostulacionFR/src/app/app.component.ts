import { Component, OnInit } from '@angular/core';
import { Cliente } from './model/cliente';
import { ClienteService } from './services/cliente.service';
import { ClientesService } from './services/clientes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'frontendpostulacionFR';

  user: Cliente;
  clientSelected: boolean;

  constructor(
    private clienteService: ClienteService,
    private clientesService: ClientesService,
    private _router: Router,
  ) { }

  ngOnInit() {
    this.getCliente();
    this.getMenu();
  }

  getMenu(){
    this.clienteService.getMenu().subscribe(menu=>{
      this.clientSelected = menu;
    });
  }

  getCliente(){
    this.clienteService.getCliente().subscribe(client=>{
      if(client){
        this.user = client;
        this.clienteService.setMenu(true);
      }else{
        this._router.navigate(['home']);
        this.clientSelected = false;
      }
    });
  }

  resetClient(){
    this.clientSelected = false;
  }
}
