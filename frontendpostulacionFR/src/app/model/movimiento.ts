export interface Movimiento {
    descripcion: string,
    num_cuenta: string,
    tipo_mov: string,
    monto: number,
    fecha: Date
}