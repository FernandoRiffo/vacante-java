export interface Cliente {
    id_cliente: number,
    nombre: string,
    apellido: string,
    num_cuenta: string,
    saldo: number,
    habilitado: boolean
}