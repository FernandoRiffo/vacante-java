export interface Producto {
    id_prod: number,
    nombre_prod: string,
    valor_prod: number
}