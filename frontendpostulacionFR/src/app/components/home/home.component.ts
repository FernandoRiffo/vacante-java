import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ClientesService } from 'src/app/services/clientes.service';
import { ClienteService } from 'src/app/services/cliente.service';
import { Router } from '@angular/router';
import { Cliente } from 'src/app/model/cliente';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit {

  displayedColumns: string[] = ['nombre', 'apellido', 'num_cuenta', 'habilitado', 'saldo', 'star'];
  dataSource = new MatTableDataSource<Cliente>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private clienteService: ClientesService,
    private clientService: ClienteService,
    private _router: Router,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.loadListClient();
    
  }

  loadListClient() {
    this.clienteService.getListClient().subscribe(data => {
      if (data != null) {
        this.dataSource = new MatTableDataSource<Cliente>(data);
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  setClient(option: number, client: Cliente) {
    switch (option) {
      case 1: {
        //statements; 
        this.clientService.setCliente(client);
        this._router.navigate(['cliente']);
        break;
      }
      case 2: {
        //statements; 
        if(client.saldo > 0){
          this.clientService.setCliente(client);
          this._router.navigate(['compras']);
        }else{
          this.openSnackBar("No cuenta con Saldo Disponible para realizar compras","");
        }
        break;
      }
      case 3: {
        //statements; 
        if (client.habilitado) {
          this.clientService.setCliente(client);
          this._router.navigate(['recargas']);
        } else {
          this.openSnackBar("Usuario No Habilitado para Realizar Recargas","");
        }
        break;
      }
      case 4: {
        //statements; 
        this.clientService.setCliente(client);
        this._router.navigate(['historico']);
        break;
      }
    }
    

  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }


}
