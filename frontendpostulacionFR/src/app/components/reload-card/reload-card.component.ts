import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/model/cliente';
import { ClienteService } from 'src/app/services/cliente.service';
import { ClientesService } from 'src/app/services/clientes.service';
import { Router } from '@angular/router';
import { OperacionesService } from 'src/app/services/operaciones.service';
import { Movimiento } from 'src/app/model/movimiento';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-reload-card',
  templateUrl: './reload-card.component.html',
  styleUrls: ['./reload-card.component.css']
})
export class ReloadCardComponent implements OnInit {

  user: Cliente;
  montoRecarga: number;

  constructor(
    private clienteService: ClienteService,
    private clientesService: ClientesService,
    private _router: Router,
    private operacionesService: OperacionesService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getCliente();
  }

  getCliente() {
    this.clienteService.getCliente().subscribe(client => {
      if (client) {
        this.user = client;
      } else {
        this._router.navigate(['home']);
      }
    });
  }

  recargar(){
    const movimiento: Movimiento = {
      descripcion: 'Recarga',
      num_cuenta: this.user.num_cuenta,
      tipo_mov: 'r',
      monto: this.montoRecarga,
      fecha: new Date()
    }
    this.operacionesService.setMovimientoRecarga(movimiento).subscribe(resp =>{
      if(resp){
        this.openSnackBar("Recarga realizada correctamente", "");
        this.clienteService.setMenu(false);
        this._router.navigate(['home']);
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
