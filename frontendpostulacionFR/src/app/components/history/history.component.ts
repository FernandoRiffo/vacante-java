import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ClientesService } from 'src/app/services/clientes.service';
import { ClienteService } from 'src/app/services/cliente.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HistoryService } from 'src/app/services/history.service';
import { CuentaRequest } from 'src/app/model/cuentaRequest';
import { Cliente } from 'src/app/model/cliente';
import { Movimiento } from 'src/app/model/movimiento';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  displayedColumns: string[] = ['id_movimiento', 'descripcion', 'compras', 'recargas', 'fecha'];
  dataSource = new MatTableDataSource<any>();
  user: Cliente;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private clientService: ClienteService,
    private _router: Router,
    private _snackBar: MatSnackBar,
    private historyService: HistoryService
  ) { }

  ngOnInit() {
    this.getCliente();
  }

  getCliente() {
    this.clientService.getCliente().subscribe(client => {
      if (client) {
        this.user = client;
        this.getHistory(client);
      } else {
        this._router.navigate(['home']);
      }
    });
  }

  getHistory(client) {
    const cuenta: CuentaRequest = {
      num_cuenta: client.num_cuenta
    }
    this.historyService.getHistory(cuenta).subscribe(data => {
      if (data != null) {
        this.obtenerTotalCompras(data);
        this.dataSource = new MatTableDataSource<any>(data);
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  obtenerTotalCompras(data) {
    let montoTotal = 0;
    let montoTotalR = 0;
    data.forEach(element => {
      if (element.tipo_mov === 'c') {
        montoTotal = montoTotal + element.monto;
      } else if (element.tipo_mov === 'r') {
        montoTotalR = montoTotalR + element.monto;
      }
    });

    const mov = {
      descripcion: 'TOTAL',
      num_cuenta: '',
      tipo_mov: 't',
      monto: montoTotal,
      montor: montoTotalR,
      fecha: null
    }

    const saldo = {
      descripcion: 'Saldo',
      num_cuenta: '',
      tipo_mov: 's',
      monto: '+' + this.user.saldo,
      fecha: null
    }


    data.push(mov);
    data.push(saldo);
  }

}
