import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/model/cliente';
import { Producto } from 'src/app/model/producto';
import { ClienteService } from 'src/app/services/cliente.service';
import { ClientesService } from 'src/app/services/clientes.service';
import { Router } from '@angular/router';
import { ProductosService } from 'src/app/services/productos.service';
import { OperacionesService } from 'src/app/services/operaciones.service';
import { Movimiento } from 'src/app/model/movimiento';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-purchases',
  templateUrl: './purchases.component.html',
  styleUrls: ['./purchases.component.css']
})
export class PurchasesComponent implements OnInit {

  user: Cliente;
  producto: Producto;
  listProd: Producto[] = [];

  constructor(
    private clienteService: ClienteService,
    private clientesService: ClientesService,
    private _router: Router,
    private productoService: ProductosService,
    private operacionesService: OperacionesService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getCliente();
    this.getProductos();
  }

  getCliente() {
    this.clienteService.getCliente().subscribe(client => {
      if (client) {
        this.user = client;
      } else {
        this._router.navigate(['home']);
      }
    });
  }

  getProductos() {
    this.productoService.getListProductos().subscribe(prod => {
      if (prod) {
        this.listProd = prod;
      }
    });
  }

  onSelectionChange(prod: Producto) {
    if (prod) {
      this.producto = prod;
    }
  }

  comprar() {
    if (this.producto.valor_prod <= this.user.saldo) {
      const movimiento: Movimiento = {
        descripcion: 'Compra de ' + this.producto.nombre_prod,
        num_cuenta: this.user.num_cuenta,
        tipo_mov: 'c',
        monto: this.producto.valor_prod,
        fecha: new Date()
      }
      this.operacionesService.setMovimientoCompra(movimiento).subscribe(resp => {
        if(resp){
          this.openSnackBar("Compra realizada correctamente", "");
          this.clienteService.setMenu(false);
          this._router.navigate(['home']);
        }
      });
    } else {
      this.openSnackBar("No cuenta con el saldo suficiente", "");
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
