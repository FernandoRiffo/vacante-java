import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Cliente } from 'src/app/model/cliente';
import { ClienteService } from 'src/app/services/cliente.service';
import { ClientesService } from 'src/app/services/clientes.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  user: Cliente;
  nombre: string;
  apellido: string;

  constructor(
    private clienteService: ClienteService,
    private clientesService: ClientesService,
    private _router: Router,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getCliente();
  }

  getCliente(){
    this.clienteService.getCliente().subscribe(client=>{
      if(client){
        this.user = client;
        this.nombre = client.nombre;
        this.apellido = client.apellido;
      }else{
        this._router.navigate(['home']);
      }
    });
  }

  saveData(){
    const updateClient: Cliente = {
      id_cliente: this.user.id_cliente,
      nombre: this.nombre,
      apellido: this.apellido,
      num_cuenta: this.user.num_cuenta,
      saldo: this.user.saldo,
      habilitado: this.user.habilitado
    }; 
    this.clienteService.updateClient(updateClient).subscribe(updClient =>{
      if(updClient){
        this.clienteService.setCliente(updClient);
        this.openSnackBar("Usuario Actualizado correctamente","");
      }
    });
  }

  reloadData(){
    this.getCliente();
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
