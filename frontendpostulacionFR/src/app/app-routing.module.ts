import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ClientComponent } from './components/client/client.component';
import { PurchasesComponent } from './components/purchases/purchases.component';
import { HistoryComponent } from './components/history/history.component';
import { ReloadCardComponent } from './components/reload-card/reload-card.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }, {
    path: 'home',
    component: HomeComponent
  }, {
    path: 'cliente',
    component: ClientComponent
  }, {
    path: "compras",
    component: PurchasesComponent
  }, {
    path: "historico",
    component: HistoryComponent
  }, {
    path: "recargas",
    component: ReloadCardComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
