import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  constructor(private http: HttpClient) { }

  getListProductos(): Observable<any> {
    const url = environment.API + environment.BASE_URL + '/productos';
    return this.http.get(url);
  }
}
