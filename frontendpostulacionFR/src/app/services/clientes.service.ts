import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  constructor(private http: HttpClient) { }

  getListClient(): Observable<any> {
    const url = environment.API + environment.BASE_URL + '/clientes';
    return this.http.get(url);
  }
}
