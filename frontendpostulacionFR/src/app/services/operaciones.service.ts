import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CuentaRequest } from '../model/cuentaRequest';
import { Movimiento } from '../model/movimiento';

@Injectable({
  providedIn: 'root'
})
export class OperacionesService {

  constructor(private http: HttpClient) { }

  setMovimientoCompra(movimiento: Movimiento): Observable<any> {
    const url = environment.API + environment.BASE_URL + '/comprar';
    return this.http.post(url, movimiento);
  }

  setMovimientoRecarga(movimiento: Movimiento): Observable<any> {
    const url = environment.API + environment.BASE_URL + '/recargar';
    return this.http.post(url, movimiento);
  }
}
