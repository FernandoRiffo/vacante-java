import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Cliente } from '../model/cliente';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private cliente = new BehaviorSubject<any>(null);
  private menu = new BehaviorSubject<any>(false);
  constructor(private http: HttpClient) { }

  setCliente(behave: Object) {
    this.cliente.next(behave);
  }
  getCliente(): Observable<any> {
    return this.cliente.asObservable();
  }

  setMenu(behave: Object) {
    this.menu.next(behave);
  }
  getMenu(): Observable<any> {
    return this.menu.asObservable();
  }

  updateClient(cliente: Cliente): Observable<any> {
    const url = environment.API + environment.BASE_URL + '/updateClient';
    return this.http.post(url, cliente);
  }
}
