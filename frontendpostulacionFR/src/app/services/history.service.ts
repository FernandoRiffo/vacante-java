import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CuentaRequest } from '../model/cuentaRequest';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  constructor(private http: HttpClient) { }

  getHistory(cuentaRequest: CuentaRequest): Observable<any> {
    const url = environment.API + environment.BASE_URL + '/history';
    return this.http.post(url, cuentaRequest);
  }
}
