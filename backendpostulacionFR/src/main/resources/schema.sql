create table cliente
(
	id_cliente integer primary key auto_increment,
	nombre varchar(20),
	apellido varchar(20),
	num_cuenta varchar(20),
	saldo integer,
	habilitado boolean default true

);

create table movimiento
(
    id_movimiento bigint auto_increment
        primary key,
    descripcion   varchar(255) null,
    fecha         date         null,
    monto         int          null,
    num_cuenta    varchar(255) null,
    tipo_mov      varchar(255) null
)

create table producto
(
	id_prod integer primary key auto_increment,
	nombre_prod varchar(20),
	valor_prod integer
);