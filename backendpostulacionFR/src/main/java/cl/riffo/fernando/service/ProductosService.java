package cl.riffo.fernando.service;

import java.util.List;

import cl.riffo.fernando.model.Producto;

public interface ProductosService {

	List<Producto> findAll();

}
