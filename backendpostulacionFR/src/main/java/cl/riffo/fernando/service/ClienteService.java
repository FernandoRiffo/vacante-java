package cl.riffo.fernando.service;

import java.util.List;

import cl.riffo.fernando.model.Cliente;

public interface ClienteService {

	List<Cliente> findAll();

	Cliente findByNumCuenta(String num_cuenta);

	Cliente save(Cliente cliente);

}
