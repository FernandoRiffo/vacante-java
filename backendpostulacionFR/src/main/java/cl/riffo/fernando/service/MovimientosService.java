package cl.riffo.fernando.service;

import java.util.List;

import cl.riffo.fernando.model.Movimiento;

public interface MovimientosService {

	List<Movimiento> findAll();

	List<Movimiento> findByNumCuenta(String num_cuenta);

	Movimiento save(Movimiento mov);

}
