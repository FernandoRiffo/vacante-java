package cl.riffo.fernando.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.riffo.fernando.model.Producto;

@Repository
public interface ProductosRepository extends JpaRepository<Producto, Long> {

}
