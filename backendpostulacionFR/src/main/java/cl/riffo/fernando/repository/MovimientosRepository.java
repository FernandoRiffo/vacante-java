package cl.riffo.fernando.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cl.riffo.fernando.model.Movimiento;

@Repository
public interface MovimientosRepository extends JpaRepository<Movimiento, Long>{
	
	@Query("select m from Movimiento m where m.num_cuenta = (?1)")
	List<Movimiento> findByNumCuenta(String num_cuenta);

}
