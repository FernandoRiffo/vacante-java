package cl.riffo.fernando.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cl.riffo.fernando.model.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>{

	@Query("select c from Cliente c where c.num_cuenta = (?1)")
	Cliente findByNumCuenta(String num_cuenta);
}
