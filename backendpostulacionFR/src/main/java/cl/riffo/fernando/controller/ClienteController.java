package cl.riffo.fernando.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.riffo.fernando.model.Cliente;
import cl.riffo.fernando.model.CuentaRequest;
import cl.riffo.fernando.model.Movimiento;
import cl.riffo.fernando.service.ClienteService;
import cl.riffo.fernando.service.MovimientosService;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ClienteController {
		
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private MovimientosService movimientosService;
	
	@GetMapping("/clientes")
	public List<Cliente> findAll(){
		return clienteService.findAll();
	}
	
	@PostMapping("/history")
	public List<Movimiento> findByNumCuenta(@RequestBody CuentaRequest cuenta){
		return movimientosService.findByNumCuenta(cuenta.getNum_cuenta());
	}
	
	@PostMapping("/updateClient")
	public Cliente save(@RequestBody Cliente cliente){
		return clienteService.save(cliente);
	}

}
