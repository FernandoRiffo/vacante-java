package cl.riffo.fernando.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.riffo.fernando.model.Movimiento;
import cl.riffo.fernando.service.MovimientosService;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class OperacionesController {

	@Autowired
	private MovimientosService movimientosService;
	
	@PostMapping("/recargar")
	public Movimiento recargar(@RequestBody Movimiento recarga){
		return movimientosService.save(recarga);
	}
	
	@PostMapping("/comprar")
	public Movimiento comprar(@RequestBody Movimiento compra){
		return movimientosService.save(compra);
	}
}
