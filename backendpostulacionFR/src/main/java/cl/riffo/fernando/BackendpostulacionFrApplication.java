package cl.riffo.fernando;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendpostulacionFrApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendpostulacionFrApplication.class, args);
	}

}
