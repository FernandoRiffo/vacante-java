package cl.riffo.fernando.model;

public class CuentaRequest {
	private String num_cuenta;

	public String getNum_cuenta() {
		return num_cuenta;
	}

	public void setNum_cuenta(String num_cuenta) {
		this.num_cuenta = num_cuenta;
	}

	@Override
	public String toString() {
		return "CuentaRequest [num_cuenta=" + num_cuenta + "]";
	}
	
	
}
