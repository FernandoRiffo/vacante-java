package cl.riffo.fernando.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "movimiento")
public class Movimiento {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_movimiento;
	private String descripcion;
	private String num_cuenta;
	private String tipo_mov;
	private Integer monto;
	private Date fecha;
	public Long getId_movimiento() {
		return id_movimiento;
	}
	public void setId_movimiento(Long id_movimiento) {
		this.id_movimiento = id_movimiento;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNum_cuenta() {
		return num_cuenta;
	}
	public void setNum_cuenta(String num_cuenta) {
		this.num_cuenta = num_cuenta;
	}
	public String getTipo_mov() {
		return tipo_mov;
	}
	public void setTipo_mov(String tipo_mov) {
		this.tipo_mov = tipo_mov;
	}
	public Integer getMonto() {
		return monto;
	}
	public void setMonto(Integer monto) {
		this.monto = monto;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	@Override
	public String toString() {
		return "Movimiento [id_movimiento=" + id_movimiento + ", descripcion=" + descripcion + ", num_cuenta="
				+ num_cuenta + ", tipo_mov=" + tipo_mov + ", monto=" + monto + ", fecha=" + fecha + "]";
	}
	
	
}
