package cl.riffo.fernando.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "producto")
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id_prod;
	private String nombre_prod;
	private Integer valor_prod;
	public Long getId_prod() {
		return id_prod;
	}
	public void setId_prod(Long id_prod) {
		this.id_prod = id_prod;
	}
	public String getNombre_prod() {
		return nombre_prod;
	}
	public void setNombre_prod(String nombre_prod) {
		this.nombre_prod = nombre_prod;
	}
	public Integer getValor_prod() {
		return valor_prod;
	}
	public void setValor_prod(Integer valor_prod) {
		this.valor_prod = valor_prod;
	}
	
	
}
