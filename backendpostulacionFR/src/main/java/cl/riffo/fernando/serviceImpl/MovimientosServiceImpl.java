package cl.riffo.fernando.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.riffo.fernando.model.Cliente;
import cl.riffo.fernando.model.Movimiento;
import cl.riffo.fernando.repository.MovimientosRepository;
import cl.riffo.fernando.service.ClienteService;
import cl.riffo.fernando.service.MovimientosService;

@Service
public class MovimientosServiceImpl implements MovimientosService {

	@Autowired
	private MovimientosRepository movimientosRepository;

	@Autowired
	private ClienteService clienteService;

	@Override
	public List<Movimiento> findAll() {
		return this.movimientosRepository.findAll();
	}

	@Override
	public List<Movimiento> findByNumCuenta(String num_cuenta) {
		return this.movimientosRepository.findByNumCuenta(num_cuenta);
	}

	@Override
	public Movimiento save(Movimiento mov) {

		Movimiento movNew = new Movimiento();
		movNew = this.movimientosRepository.save(mov);
		updateSaldo(movNew);
		return movNew;
	}

	private void updateSaldo(Movimiento movNew) {
		Integer nuevoSaldo = null;
		Cliente updateCliente = new Cliente();
		updateCliente = this.clienteService.findByNumCuenta(movNew.getNum_cuenta());
		if (movNew.getTipo_mov().equals("c")) {
			nuevoSaldo = updateCliente.getSaldo() - movNew.getMonto();
			updateCliente.setSaldo(nuevoSaldo);
		} else if (movNew.getTipo_mov().equals("r")) {
			nuevoSaldo = updateCliente.getSaldo() + movNew.getMonto();
			updateCliente.setSaldo(nuevoSaldo);
		}

		this.clienteService.save(updateCliente);

	}

}
