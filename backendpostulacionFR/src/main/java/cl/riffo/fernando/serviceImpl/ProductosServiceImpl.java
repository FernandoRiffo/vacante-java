package cl.riffo.fernando.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.riffo.fernando.model.Producto;
import cl.riffo.fernando.repository.ProductosRepository;
import cl.riffo.fernando.service.ProductosService;

@Service
public class ProductosServiceImpl implements ProductosService{

	@Autowired
	private ProductosRepository productosRepository;

	@Override
	public List<Producto> findAll() {
		return this.productosRepository.findAll();
	}
}
