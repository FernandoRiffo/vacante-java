package cl.riffo.fernando.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.riffo.fernando.model.Cliente;
import cl.riffo.fernando.repository.ClienteRepository;
import cl.riffo.fernando.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	public List<Cliente> findAll() {
		return this.clienteRepository.findAll();
	}
	
	@Override
	public Cliente findByNumCuenta(String num_cuenta){
		return this.clienteRepository.findByNumCuenta(num_cuenta);
	}
	
	@Override
	public Cliente save(Cliente cliente){
		return this.clienteRepository.save(cliente);
	}
}
